#!/bin/bash
docker kill $(docker ps -q)
docker plugin install grafana/loki-docker-driver:latest --alias loki --grant-all-permissions
cd ./traefik/
docker-compose up -d

cd ../loki/
docker-compose up -d

cd ../keycloak/
docker-compose up -d

cd ../joomla/
docker-compose up -d

cd ../matomo/
docker-compose up -d